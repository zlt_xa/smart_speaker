#include <stdio.h>
#include <stdlib.h> //malloc使用的头文件
#include <curl/curl.h> //libcurl的头文件
#include "cJSON.h" //cJSON的头文件

int main(void)
{
    //以只写方式打开文件
    FILE* fp = fopen("hello.txt", "w");
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("fopen() failed");
        return 1;
    }

    //初始化libcurl
    CURL* curl = curl_easy_init();
    if (curl == NULL)
    {
        perror("curl_easy_init() failed");
        return 1;
    }

    //准备HTTP请求消息，设置API地址（URI）
    curl_easy_setopt(curl, CURLOPT_URL, "https://coronavirus-tracker-api.herokuapp.com/v2/latest");
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

    //发送HTTP请求消息
    CURLcode error = curl_easy_perform(curl);
    if (error != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(error));
        curl_easy_cleanup(curl);
        return 1;
    }

    //释放libcurl申请的资源
    curl_easy_cleanup(curl);

    //关闭文件
    fclose(fp);

    //以只读方式打开文件
    fp = fopen("hello.txt", "r");
    if (fp == NULL) //打开文件失败，打印错误信息并退出
    {
        perror("fopen() failed");
        return 1;
    }

    //将文件指针定位到文件末尾
    fseek(fp, 0, SEEK_END);
    //获取文件指针的当前位置，即文件的大小
    long size = ftell(fp);
    //将文件指针重新定位到文件头
    fseek(fp, 0, SEEK_SET);

    //分配文件大小相同的内存空间
    char* jsonstr = malloc(size);

    //将文件中的内容读取到内存中
    fread(jsonstr, 1, size, fp);

    fclose(fp);

    //解析JSON字符串
    cJSON* json = cJSON_Parse(jsonstr);

    cJSON* latest = cJSON_GetObjectItemCaseSensitive(json, "latest");

    cJSON* comfirmed = cJSON_GetObjectItemCaseSensitive(latest, "confirmed");

    cJSON* deaths = cJSON_GetObjectItemCaseSensitive(latest, "deaths");

    printf("确诊人数: %d\n", comfirmed->valueint);
    printf("死亡人数: %d\n", deaths->valueint);

    //释放json数据结构占用的内存
    cJSON_free(json);

    return 0;
}
